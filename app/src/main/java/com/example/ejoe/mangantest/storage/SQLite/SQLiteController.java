package com.example.ejoe.mangantest.storage.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ejoe.mangantest.utilities.Constants;
import com.example.ejoe.mangantest.model.Answer;
import com.example.ejoe.mangantest.model.Question;
import com.example.ejoe.mangantest.utilities.QuestionAnswerAdapter;

import java.util.ArrayList;

/**
 * A controller that manages the communication between the app and the database
 */
public class SQLiteController {

    /**
     * The database helper
     */
    private ManganTestSQLite mtdb;
    /**
     * The database object
     */
    private SQLiteDatabase db;
    /**
     * The instance of the class
     */
    private static SQLiteController instance = null;

    /**
     * Creates a SQLiteController object
     *
     * @param context the context of the activity
     */
    private SQLiteController(Context context) {
        this.mtdb = new ManganTestSQLite(context, Constants.DB, null, Constants.VERSION);
    }

    /**
     * If the instance of the class is null, it calls the constructor, then returns the instance
     * of the class
     *
     * @param context the Context object of the activity where this method is called
     * @return the instance of the class
     */
    public static SQLiteController getInstance(Context context) {
        if (instance == null) {
            instance = new SQLiteController(context);
        }

        return instance;
    }

    /**
     * Creates the connection with the database
     *
     * @return a SQLiteDatabase object
     */
    private void connection() {
        db = mtdb.getWritableDatabase();
    }

    /**
     * Closes the connection with the database
     */
    public void close() {
        db.close();
    }

    /**
     * Save a question in the database
     *
     * @param q the question to save
     */
    public long saveQuestion(Question q) {

        connection();

        ContentValues values = QuestionAnswerAdapter.generateContentValues(q);

        long rowId = db.insert(Constants.QUESTION_TABLE, null, values);

        close();

        return rowId;
    }

    /**
     * Returns the questions stored in the database
     *
     * @return an array of Question objects
     */
    public ArrayList<Question> getQuestions() {

        connection();

        ArrayList<Question> savedQuestions = new ArrayList<>();

        String[] columns = {"rowId", "statement", "category", "correctAnswer", "incorrectAnswer1",
                "incorrectAnswer2", "incorrectAnswer3", "image"};
        Cursor c = db.query(Constants.QUESTION_TABLE, columns, null, null,
                null, null, columns[1]);

        Question q;

        if (c.moveToFirst()) {
            do {

                if (c.getString(7) != null) {
                    q = new Question(
                            c.getString(0),
                            c.getString(1),
                            c.getString(2),
                            new Answer(c.getString(3), true),
                            new Answer(c.getString(4), false),
                            new Answer(c.getString(5), false),
                            new Answer(c.getString(6), false),
                            c.getString(7));
                } else {
                    q = new Question(
                            c.getString(0),
                            c.getString(1),
                            c.getString(2),
                            new Answer(c.getString(3), true),
                            new Answer(c.getString(4), false),
                            new Answer(c.getString(5), false),
                            new Answer(c.getString(6), false),
                            null);
                }

                savedQuestions.add(q);

            } while (c.moveToNext());
        }

        c.close();

        close();

        return savedQuestions;
    }

    /**
     * Returns the categories of the questions stored in the database
     *
     * @return an ArrayList containing the categories
     */
    public ArrayList<String> getCategories() {

        connection();

        String[] columns = {"category"};

        Cursor c = db.query(true, Constants.QUESTION_TABLE, columns, null,
                null, null, null, null, null);

        String s;
        ArrayList<String> categories = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                s = c.getString(0);
                categories.add(s);
            } while (c.moveToNext());
        }
        c.close();

        close();

        return categories;
    }

    /**
     * Returns a question stored in the database
     *
     * @param rowId the id of the Question
     * @return a Question object
     */
    public Question getQuestion(String rowId) {

        connection();

        Question q = null;
        String[] columns = {"*"};
        String[] selectionArgs = {rowId};

        Cursor c = db.query(Constants.QUESTION_TABLE, columns, "rowId=?", selectionArgs,
                null, null, null);

        if (c.moveToFirst()) {
            q = new Question(rowId, c.getString(0),
                    c.getString(1), new Answer(c.getString(2), true),
                    new Answer(c.getString(3), false), new Answer(c.getString(
                    4), false), new Answer(c.getString(5), false),
                    c.getString(6));
        }
        c.close();

        close();

        return q;
    }

    /**
     * Update a question stored in the database
     *
     * @param q the question containing the new information
     * @return the number of rows affected
     */
    public int updateQuestion(Question q, String rowId) {

        connection();

        ContentValues values = QuestionAnswerAdapter.generateContentValues(q);

        String[] args = {rowId};

        int rows = db.update(Constants.QUESTION_TABLE, values, "rowId=?", args);

        close();

        return rows;
    }

    public int deleteQuestion(String id){
        connection();

        String[] args = {id};

        int rows = db.delete(Constants.QUESTION_TABLE, "rowId=?", args);

        close();

        return rows;
    }

    public int getQuestionsCount(){
        return getQuestions().size();
    }

    public int getCategoriesCount(){
        return getCategories().size();
    }
}

package com.example.ejoe.mangantest.utilities;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.util.Xml;

import com.example.ejoe.mangantest.model.Answer;
import com.example.ejoe.mangantest.model.Question;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 * A class used to handle the Question and Answer objects.
 */
public class QuestionAnswerAdapter {

    /**
     * Checks if the argument for creating the Question object is valid
     *
     * @param
     * @return false if the sentence is empty or null or true otherwise
     */
    public static boolean[] checkQuestion(String statement, String correctAnswer, String incorrectAnswer1,
                                          String incorrectAnswer2, String incorrectAnswer3, String category) {

        boolean[] checkQuestion = new boolean[6];

        Question q = new Question();
        Answer a = new Answer();

        try {
            q.setStatement(statement);
            checkQuestion[0] = true;
        } catch (NullPointerException e) {
            checkQuestion[0] = false;
        }

        try {
            a.setAnswer(correctAnswer);
            checkQuestion[1] = true;
        } catch (NullPointerException e) {
            checkQuestion[1] = false;
        }

        try {
            a.setAnswer(incorrectAnswer1);
            checkQuestion[2] = true;
        } catch (NullPointerException e) {
            checkQuestion[2] = false;
        }

        try {
            a.setAnswer(incorrectAnswer2);
            checkQuestion[3] = true;
        } catch (NullPointerException e) {
            checkQuestion[3] = false;
        }

        try {
            a.setAnswer(incorrectAnswer3);
            checkQuestion[4] = true;
        } catch (NullPointerException e) {
            checkQuestion[4] = false;
        }

        try {
            q.setCategory(category);
            checkQuestion[5] = true;
        } catch (NullPointerException e) {
            checkQuestion[5] = false;
        }

        return checkQuestion;
    }

    /**
     * Creates a ContentValues object from a Question object that is going to be stored in the
     * database
     *
     * @param q the question to be saved in the database
     * @return a ContentValues object containing the data from the Question
     */
    public static ContentValues generateContentValues(Question q) {

        ContentValues values = new ContentValues(7);

        values.put("statement", q.getStatement());
        values.put("category", q.getCategory());

        for (Answer a : q.getAnswers()) {
            if (a.isCorrect()) {
                values.put("correctAnswer", a.getAnswer());
            }
        }

        int count = 1;
        String column;

        for (Answer a : q.getAnswers()) {

            column = "incorrectAnswer";

            if (!a.isCorrect()) {

                column = column.concat(String.valueOf(count));
                values.put(column, a.getAnswer());
                count++;
            }
        }

        values.put("image", q.getImage());

        return values;
    }

    public static String toMoodleXml(ArrayList<Question> listaPreguntas, ArrayList<String> listaCategorias) {
        String moodleXml = "";

        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try {
            xmlSerializer.setOutput(writer);
            //Start Document
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            //Open Tag <file>
            xmlSerializer.startTag("", "quiz");

            for (Question q : listaPreguntas) {
                xmlSerializer.startTag("", "question");
                xmlSerializer.attribute("", "type", "category");
                xmlSerializer.startTag("", "category");
                xmlSerializer.startTag("", "text");
                xmlSerializer.text(q.getCategory());
                xmlSerializer.endTag("", "text");
                xmlSerializer.endTag("", "category");
                xmlSerializer.endTag("", "question");
                xmlSerializer.startTag("", "question");
                xmlSerializer.attribute("", "type", "multichoice");
                xmlSerializer.startTag("", "name");

                xmlSerializer.startTag("", "text");
                xmlSerializer.text(q.getStatement());
                xmlSerializer.endTag("", "text");
                xmlSerializer.endTag("", "name");

                xmlSerializer.startTag("", "questiontext");
                xmlSerializer.attribute("", "format", "html");

                if (q.getImage() != null) {
                    xmlSerializer.startTag("", "text");
                    xmlSerializer.text("<![CDATA[<p>" + q.getStatement() +
                            "</p><p><img src=\"@@PLUGINFILE@@/imagen_pregunta.jpg\" /></p>" +
                            "]]>");
                    xmlSerializer.endTag("", "text");

                    xmlSerializer.startTag("", "file");
                    xmlSerializer.attribute("", "name", "imagen_pregunta.jpg");
                    xmlSerializer.attribute("", "path", "/");
                    xmlSerializer.attribute("", "encoding", "base64");
                    xmlSerializer.text(q.getImage());
                    xmlSerializer.endTag("", "file");
                } else {
                    xmlSerializer.startTag("", "text");
                    xmlSerializer.text("<![CDATA[" + q.getStatement() + "]]>");
                    xmlSerializer.endTag("", "text");
                }

                xmlSerializer.endTag("", "questiontext");

                xmlSerializer.startTag("", "answernumbering");
                xmlSerializer.text("abc");
                xmlSerializer.endTag("", "answernumbering");

                for (Answer a : q.getAnswers()) {
                    xmlSerializer.startTag("", "answer");

                    if (a.isCorrect()) {
                        xmlSerializer.attribute("", "fraction", "100");
                    } else {
                        xmlSerializer.attribute("", "fraction", "0");
                    }
                    xmlSerializer.attribute("", "format", "html");

                    xmlSerializer.startTag("", "text");
                    xmlSerializer.text(a.getAnswer());
                    xmlSerializer.endTag("", "text");

                    xmlSerializer.endTag("", "answer");
                }
                xmlSerializer.endTag("", "question");
            }
            xmlSerializer.endTag("", "quiz");
            xmlSerializer.endDocument();

            moodleXml = writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return moodleXml;
    }

    public static ArrayList<Question> fromMoodleXml(Intent receivedIntent, ContentResolver contentResolver, String tag) {
        String category = null;
        String statement = null;
        String image = null;
        Question question;
        ArrayList<Answer> answers = new ArrayList<>();
        ArrayList<Question> questions = new ArrayList<>();
        Uri data = receivedIntent.getParcelableExtra(Intent.EXTRA_STREAM);

        try (InputStream fis = contentResolver.openInputStream(data)) {
            XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
            xppf.setNamespaceAware(false);
            XmlPullParser xpp = xppf.newPullParser();
            xpp.setInput(fis, null);
            int evenType = xpp.getEventType();
            while (evenType != XmlPullParser.END_DOCUMENT) {
                switch (evenType) {
                    case XmlPullParser.START_DOCUMENT:

                        MyLog.d(tag, "Start document");

                        break;

                    case XmlPullParser.END_DOCUMENT:

                        MyLog.d(tag, "End document");

                        break;

                    case XmlPullParser.START_TAG:

                        switch (xpp.getName()) {

                            case "question":

                                if (xpp.getAttributeValue(0).equals("category")) {

                                    MyLog.d(tag, "Category found");
                                    xpp.nextTag();
                                    xpp.nextTag();
                                    xpp.next();
                                    category = xpp.getText();

                                } else if (xpp.getAttributeValue(0).equals("multichoice")) {

                                    MyLog.d(tag, "Statement found");
                                    xpp.nextTag();
                                    xpp.nextTag();
                                    xpp.next();
                                    statement = xpp.getText();

                                }

                                break;

                            case "file":

                                MyLog.d(tag, "Image found");
                                xpp.next();
                                image = xpp.getText();

                                break;

                            case "answer":

                                System.out.println(xpp.getName());

                                if (xpp.getAttributeValue(0).equals("100")) {
                                    MyLog.d(tag, "Correct answer found");
                                    xpp.nextTag();
                                    xpp.next();
                                    answers.add(new Answer(xpp.getText(), true));

                                } else if (xpp.getAttributeValue(0).equals("0")) {
                                    MyLog.d(tag, "Incorrect answer found");
                                    xpp.nextTag();
                                    xpp.next();
                                    answers.add(new Answer(xpp.getText(), false));

                                    if (answers.size() == 4) {
                                        MyLog.d(tag, "Found all needed data");

                                        question = new Question(statement, category,
                                                answers.get(0), answers.get(1), answers.get(2),
                                                answers.get(3), image);
                                        questions.add(question);
                                        MyLog.d(tag, "Question added");
                                        MyLog.d(tag, "Resetting variables");
                                        category = null;
                                        statement = null;
                                        image = null;
                                        answers.clear();
                                    }
                                }
                                break;
                        }
                        break;
                }
                evenType = xpp.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
            questions = null;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            questions = null;
        } catch (NullPointerException e) {
            e.printStackTrace();
            questions = null;
        }

        return questions;
    }
}

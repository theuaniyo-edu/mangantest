package com.example.ejoe.mangantest.presentation.UI;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.ejoe.mangantest.storage.SQLite.SQLiteController;
import com.example.ejoe.mangantest.presentation.presenters.AdapterRecViewQuestions;
import com.example.ejoe.mangantest.utilities.Constants;
import com.example.ejoe.mangantest.utilities.MyLog;
import com.example.ejoe.mangantest.R;
import com.example.ejoe.mangantest.model.Question;
import com.example.ejoe.mangantest.utilities.QuestionAnswerAdapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Objects;

public class QuestionsActivity extends AppCompatActivity {

    private final String TAG = "QuestionsActivity";
    private SQLiteController DB_DRIVER = SQLiteController.getInstance(this);
    private AdapterRecViewQuestions adapter;
    private AlertDialog.Builder alertDialog;
    private View view;
    private RecyclerView rcQuestions;
    private Paint p = new Paint();
    private ArrayList<Question> storedQuestions;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case Constants.CODE_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permiso aceptado
                    Snackbar.make(findViewById(android.R.id.content), getResources().getString(
                            R.string.write_permission_granted), Snackbar.LENGTH_LONG).show();
                    TextView et = findViewById(R.id.no_questions_added);
                    et.setText(R.string.no_questions_added);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_questions_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toXml:
                String xmlContent = QuestionAnswerAdapter.toMoodleXml(
                        DB_DRIVER.getQuestions(), DB_DRIVER.getCategories());
                try {
                    OutputStreamWriter osw = new OutputStreamWriter(
                            openFileOutput("questions_for_moodle.xml", Context.MODE_PRIVATE));
                    osw.write(xmlContent);
                    osw.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return super.onNavigateUp();
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                MyLog.d(TAG, String.valueOf(position));

                //Remove questions
                if (direction == ItemTouchHelper.LEFT) {
                    initDialog(position);
                    alertDialog.show();
                } else {
                    //Edit question

                    Intent intent = new Intent(QuestionsActivity.this,
                            AddEditQuestionActivity.class);

                    Bundle bundle = new Bundle();

                    bundle.putString("id", storedQuestions.get(position).getId());

                    intent.putExtras(bundle);

                    startActivity(intent);
                }

            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#388E3C"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_edit_white);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rcQuestions);
    }

    private void initDialog(int position) {
        final String id = storedQuestions.get(position).getId();
        alertDialog = new AlertDialog.Builder(this);
        view = getLayoutInflater().inflate(R.layout.dialog_delete_question, null);
        alertDialog.setView(view);
        alertDialog.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (DB_DRIVER.deleteQuestion(id) == 1){

                    storedQuestions = DB_DRIVER.getQuestions();
                    adapter = new AdapterRecViewQuestions(storedQuestions);
                    rcQuestions.setAdapter(adapter);
                    rcQuestions.setLayoutManager(new LinearLayoutManager(QuestionsActivity.this));

                    Snackbar.make(findViewById(R.id.constraint_layout_questions_activity),
                            getResources().getString(R.string.question_deleted),
                            Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(findViewById(R.id.constraint_layout_questions_activity),
                            getResources().getString(R.string.error_deleting_question),
                            Snackbar.LENGTH_LONG).show();
                }

                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyLog.d(TAG, "Starting onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        rcQuestions = findViewById(R.id.recView_questions);
        rcQuestions.setVisibility(View.GONE);
        TextView emptyQuestions = findViewById(R.id.no_questions_added);
        emptyQuestions.setVisibility(View.GONE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        //Add UP button
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.question_list));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            MyLog.d(TAG, "Error loading toolbar");
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_questions);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AddEditQuestionActivity.class));
            }
        });

        MyLog.d(TAG, "Finishing onCreate");

    }

    @Override
    protected void onStart() {

        MyLog.d(TAG, "Starting onStart");

        super.onStart();

        MyLog.d(TAG, "Finishing onStart");
    }

    @Override
    protected void onResume() {

        MyLog.d(TAG, "Starting onResume");

        super.onResume();

        rcQuestions.setVisibility(View.GONE);
        TextView emptyQuestions = findViewById(R.id.no_questions_added);
        emptyQuestions.setVisibility(View.GONE);

            //Getting the saved questions from DB
            try {

                storedQuestions = DB_DRIVER.getQuestions();

                //If the method get one or more rows shows them on screen
                if (storedQuestions.size() > 0) {

                    adapter = new AdapterRecViewQuestions(storedQuestions);

                    rcQuestions.setAdapter(adapter);
                    rcQuestions.setLayoutManager(new LinearLayoutManager(this));
                    rcQuestions.setVisibility(View.VISIBLE);
                    initSwipe();
                } else {
                    //If the array is empty shows a message on screen
                    emptyQuestions.setVisibility(View.VISIBLE);
                }

            } catch (SQLException ex) {
                //Shows if there's an error with the database
                MyLog.d(TAG, "Database error: " + ex);
                Snackbar.make(findViewById(android.R.id.content), R.string.db_error,
                        Snackbar.LENGTH_LONG).show();
            }

        MyLog.d(TAG, "Finishing onResume");
    }

    @Override
    protected void onPause() {

        MyLog.d(TAG, "Starting onPause");

        super.onPause();

        MyLog.d(TAG, "Finishing onPause");
    }

    @Override
    protected void onStop() {

        MyLog.d(TAG, "Starting onStop");

        super.onStop();

        MyLog.d(TAG, "Finishing onStop");
    }

    @Override
    protected void onDestroy() {

        MyLog.d(TAG, "Starting onDestroy");

        super.onDestroy();

        MyLog.d(TAG, "Finishing onDestroy");
    }
}

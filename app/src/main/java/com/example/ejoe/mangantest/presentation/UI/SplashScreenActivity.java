package com.example.ejoe.mangantest.presentation.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.ejoe.mangantest.utilities.MyLog;
import com.example.ejoe.mangantest.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {

    private final String TAG = "SplashScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyLog.d(TAG, "Iniciando onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Animation animFade = AnimationUtils.loadAnimation(this, R.anim.animation);
        animFade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startActivity(new Intent(getApplicationContext(), ResumeActivity.class));
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        ImageView imageView = findViewById(R.id.splashScreen_image);

        imageView.setAnimation(animFade);

        MyLog.d(TAG, "Finalizando onCreate");
    }

    @Override
    protected void onStart() {

        MyLog.d(TAG, "Iniciando onStart");

        super.onStart();

        MyLog.d(TAG, "Finalizando onStart");
    }

    @Override
    protected void onResume() {

        MyLog.d(TAG, "Iniciando onResume");

        super.onResume();

        MyLog.d(TAG, "Finalizando onResume");
    }

    @Override
    protected void onPause() {

        MyLog.d(TAG, "Iniciando onPause");

        super.onPause();

        MyLog.d(TAG, "Finalizando onPause");
    }

    @Override
    protected void onStop() {

        MyLog.d(TAG, "Iniciando onStop");

        super.onStop();

        MyLog.d(TAG, "Finalizando onStop");
    }

    @Override
    protected void onDestroy() {

        MyLog.d(TAG, "Iniciando onDestroy");

        super.onDestroy();

        MyLog.d(TAG, "Finalizando onDestroy");
    }
}
package com.example.ejoe.mangantest.presentation.UI;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.ejoe.mangantest.model.Question;
import com.example.ejoe.mangantest.storage.SQLite.SQLiteController;
import com.example.ejoe.mangantest.utilities.Constants;
import com.example.ejoe.mangantest.utilities.MyLog;
import com.example.ejoe.mangantest.R;
import com.example.ejoe.mangantest.utilities.QuestionAnswerAdapter;
import com.example.ejoe.mangantest.utilities.Utilities;

import java.util.ArrayList;

public class ResumeActivity extends AppCompatActivity {

    private final String TAG = "ResumeActivity";
    private final SQLiteController DB_DRIVER = SQLiteController.getInstance(this);

    public boolean importFromXml(Intent receivedIntent) {
        boolean success = false;
        ArrayList<Question> importedQuestions = QuestionAnswerAdapter
                .fromMoodleXml(receivedIntent, getContentResolver(), TAG);

        if (importedQuestions != null) {
            MyLog.d(TAG, "TOTAL QUESTIONS: " + importedQuestions.size());
            for (Question q : importedQuestions) {
                DB_DRIVER.saveQuestion(q);
                success = true;
            }
        }
        return success;
    }
    //Add a menu to the toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.resume_menu, menu);
        return true;
    }

    //Add functionality to the menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_questions:
                MyLog.i("ActionBar", "Questions list");
                startActivity(new Intent(getApplicationContext(), QuestionsActivity.class));
                return true;
            case R.id.action_settings:
                MyLog.i("ActionBar", "SettingsFragment");
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            case R.id.action_about:
                MyLog.i("ActionBar", "About");
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return super.onNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyLog.d(TAG, "Starting onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_resume);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fButton_resume);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, R.string.not_implemented, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }

        //get the received intent
        Intent receivedIntent = getIntent();

        //get the action
        String receivedAction = receivedIntent.getAction();

        if (receivedAction != null) {

            // make sure it's an action and type we can handle
            if (receivedAction.equals(Intent.ACTION_SEND)) {

                if (Utilities.checkWritePermission(this)) {

                    if (importFromXml(receivedIntent)) {
                        MyLog.d(TAG, "Import questions from xml: success");
                        getIntent().setData(null);
                    } else {
                        MyLog.d(TAG, "Import questions from xml: failed");
                        getIntent().setData(null);
                    }

                } else {
                    Utilities.askPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Constants.CODE_READ_EXTERNAL_STORAGE_PERMISSION);

                    if (Utilities.checkWritePermission(this)) {
                        Snackbar.make(findViewById(android.R.id.content),
                                R.string.read_permission_granted, Snackbar.LENGTH_LONG).show();

                        if (importFromXml(receivedIntent)) {
                            MyLog.d(TAG, "Import questions from xml: success");
                            getIntent().setData(null);
                        } else {
                            MyLog.d(TAG, "Import questions from xml: failed");
                            getIntent().setData(null);
                        }

                    } else {
                        Snackbar.make(findViewById(android.R.id.content),
                                R.string.read_permission_denied, Snackbar.LENGTH_LONG).show();
                    }
                }
            } else if (receivedAction.equals(Intent.ACTION_MAIN)) {
                //app has been launched directly, not from share list
                System.out.println("action main");
            }
        }

        MyLog.d(TAG, "Finishing onCreate");
    }

    @Override
    protected void onStart() {

        MyLog.d(TAG, "Starting onStart");

        super.onStart();

        MyLog.d(TAG, "Finishing onStart");
    }

    @Override
    protected void onResume() {

        MyLog.d(TAG, "Starting onResume");

        super.onResume();

        TextView questionsCount = findViewById(R.id.questions_count);
        TextView categoriesCount = findViewById(R.id.categories_count);

        questionsCount.setText(String.valueOf(DB_DRIVER.getQuestionsCount()));
        categoriesCount.setText(String.valueOf(DB_DRIVER.getCategoriesCount()));

        MyLog.d(TAG, "Finishing onResume");
    }

    @Override
    protected void onPause() {

        MyLog.d(TAG, "Starting onPause");

        super.onPause();

        MyLog.d(TAG, "Finishing onPause");
    }

    @Override
    protected void onStop() {

        MyLog.d(TAG, "Starting onStop");

        super.onStop();

        MyLog.d(TAG, "Finishing onStop");
    }

    @Override
    protected void onDestroy() {

        MyLog.d(TAG, "Starting onDestroy");

        super.onDestroy();

        MyLog.d(TAG, "Finishing onDestroy");
    }
}

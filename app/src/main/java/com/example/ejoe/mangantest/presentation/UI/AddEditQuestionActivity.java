package com.example.ejoe.mangantest.presentation.UI;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ejoe.mangantest.model.Answer;
import com.example.ejoe.mangantest.model.Question;
import com.example.ejoe.mangantest.storage.SQLite.SQLiteController;
import com.example.ejoe.mangantest.utilities.Constants;
import com.example.ejoe.mangantest.utilities.MyLog;
import com.example.ejoe.mangantest.R;
import com.example.ejoe.mangantest.utilities.QuestionAnswerAdapter;
import com.example.ejoe.mangantest.utilities.Utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AddEditQuestionActivity extends AppCompatActivity {

    private final String TAG = "AddEditQuestionActivity";
    private final Context CONTEXT = this;
    private final SQLiteController DB_DRIVER = SQLiteController.getInstance(this);
    private boolean edit;
    private Uri uri;
    private AlertDialog.Builder alertDialog;
    private boolean addedImage;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_edit_question_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_save:

                MyLog.d(TAG, "Save button pressed");

                View view = findViewById(R.id.constraint_layout_addQuestion);
                Bundle bundle = this.getIntent().getExtras();

                final Spinner categoriesSpinner = findViewById(R.id.spinner_categories);

                final EditText statement = findViewById(R.id.question_input);
                final EditText correctAnswer = findViewById(R.id.correct_answer_input);
                final EditText incorrectAnswer1 = findViewById(R.id.incorrect_answer_1_input);
                final EditText incorrectAnswer2 = findViewById(R.id.incorrect_answer_2_input);
                final EditText incorrectAnswer3 = findViewById(R.id.incorrect_answer_3_input);
                final ImageView imageView = findViewById(R.id.loadedImage);

                final TextView spinnerError = (TextView) categoriesSpinner.getSelectedView();

                //Hides the soft keyboard
                if (getCurrentFocus() != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context
                            .INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }

                boolean emptyField = false;
                boolean checkFields[] = QuestionAnswerAdapter.checkQuestion(
                        statement.getText().toString(),
                        correctAnswer.getText().toString(),
                        incorrectAnswer1.getText().toString(),
                        incorrectAnswer2.getText().toString(),
                        incorrectAnswer3.getText().toString(),
                        categoriesSpinner.getSelectedItem().toString()
                );

                for (int i = 0; i < checkFields.length; i++) {
                    if (!checkFields[i]) {
                        emptyField = true;

                        switch (i) {
                            case 0:
                                MyLog.d(TAG, "Question check failed");
                                statement.setError(getResources().getString(R.string.required_field));
                                break;
                            case 1:
                                MyLog.d(TAG, "Correct answer check failed");
                                correctAnswer.setError(getResources().getString(R.string.required_field));
                                break;
                            case 2:
                                MyLog.d(TAG, "Incorrect answer 1 failed");
                                incorrectAnswer1.setError(getResources().getString(R.string.required_field));
                                break;
                            case 3:
                                MyLog.d(TAG, "Incorrect answer 2 failed");
                                incorrectAnswer2.setError(getResources().getString(R.string.required_field));
                                break;
                            case 4:
                                MyLog.d(TAG, "Incorrect answer 3 failed");
                                incorrectAnswer3.setError(getResources().getString(R.string.required_field));
                                break;
                            case 5:
                                spinnerError.setError("");
                                spinnerError.setTextColor(Color.RED);
                                spinnerError.setText(getResources().getString(R.string.required_field));
                                break;
                        }
                    }
                }

                //Check if there aren't empty fields
                if (!emptyField) {

                    //When we are not editing saves a new question into the database
                    if (!edit) {

                        try {

                            if (addedImage) {
                                MyLog.d(TAG, "Question with image");

                                DB_DRIVER.saveQuestion(new Question(
                                        statement.getText().toString(),
                                        categoriesSpinner.getSelectedItem().toString(),
                                        new Answer(correctAnswer.getText().toString(), true),
                                        new Answer(incorrectAnswer1.getText().toString(), false),
                                        new Answer(incorrectAnswer2.getText().toString(), false),
                                        new Answer(incorrectAnswer3.getText().toString(), false),
                                        Utilities.imgToBase64(((BitmapDrawable) imageView.getDrawable()).getBitmap())
                                ));
                            } else {
                                MyLog.d(TAG, "Question without image");

                                DB_DRIVER.saveQuestion(new Question(
                                        statement.getText().toString(),
                                        categoriesSpinner.getSelectedItem().toString(),
                                        new Answer(correctAnswer.getText().toString(), true),
                                        new Answer(incorrectAnswer1.getText().toString(), false),
                                        new Answer(incorrectAnswer2.getText().toString(), false),
                                        new Answer(incorrectAnswer3.getText().toString(), false),
                                        null)
                                );
                            }

                            MyLog.d(TAG, "Question saved");

                            //Show a snackbar confirming changes
                            Snackbar.make(view, R.string.db_insert_ok, Snackbar.LENGTH_SHORT).show();

                            //Deactivates the button
                            item.setEnabled(false);

                            //After three seconds the activity finishes
                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 3000);

                        } catch (SQLException ex) {

                            //Shows a snackbar if there's an error with the database
                            MyLog.d(TAG, "Database error: " + ex);
                            Snackbar.make(view, R.string.db_error, Snackbar.LENGTH_LONG)
                                    .show();
                        }

                        //If we are editing a question updates its values in the database
                    } else {

                        try {

                            Question q;

                            if (addedImage) {
                                MyLog.d(TAG, "Question with image");

                                q = new Question(
                                        statement.getText().toString(),
                                        categoriesSpinner.getSelectedItem().toString(),
                                        new Answer(correctAnswer.getText().toString(), true),
                                        new Answer(incorrectAnswer1.getText().toString(), false),
                                        new Answer(incorrectAnswer2.getText().toString(), false),
                                        new Answer(incorrectAnswer3.getText().toString(), false),
                                        Utilities.imgToBase64(((BitmapDrawable) imageView.getDrawable()).getBitmap())
                                );
                            } else {
                                MyLog.d(TAG, "Question without image");

                                q = new Question(
                                        statement.getText().toString(),
                                        categoriesSpinner.getSelectedItem().toString(),
                                        new Answer(correctAnswer.getText().toString(), true),
                                        new Answer(incorrectAnswer1.getText().toString(), false),
                                        new Answer(incorrectAnswer2.getText().toString(), false),
                                        new Answer(incorrectAnswer3.getText().toString(), false),
                                        null);
                            }

                            //If the methods returns 1 the update is correct
                            if (DB_DRIVER.updateQuestion(q, bundle.getString("id")) == 1) {

                                //Show a snackbar confirming changes
                                MyLog.d(TAG, "Question updated");
                                Snackbar.make(view, R.string.db_insert_ok, Snackbar.LENGTH_SHORT).show();

                                //Deactivates the button
                                item.setEnabled(false);

                                //After three seconds the activity finishes
                                new Timer().schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        MyLog.d(TAG, "Finishing activity...");
                                        finish();
                                    }
                                }, 3000);

                                //If not the app shows a snackbar notifying the error
                            } else {
                                MyLog.d(TAG, "Update error");
                                Snackbar.make(view, R.string.update_error, Snackbar.LENGTH_LONG)
                                        .show();
                            }

                        } catch (SQLException ex) {

                            //Shows if there's an error with the database
                            MyLog.d(TAG, "Database error: " + ex);
                            Snackbar.make(view, R.string.db_error, Snackbar.LENGTH_LONG).show();

                        } catch (NullPointerException ex) {

                            MyLog.d(TAG, "Update error: " + ex);
                            Snackbar.make(view, R.string.update_error, Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
                break;

            case R.id.action_camera:
                MyLog.d(TAG, "Camera button pressed");
                if (!Utilities.checkCameraPermission(CONTEXT) && !Utilities.checkWritePermission(CONTEXT)) {
                    String permissions[] = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    Utilities.askMultiplePermissions(AddEditQuestionActivity.this, permissions);
                } else if (!Utilities.checkCameraPermission(CONTEXT)) {
                    Utilities.askPermission(AddEditQuestionActivity.this,
                            Manifest.permission.CAMERA, Constants.REQUEST_CAPTURE_IMAGE);
                } else if (!Utilities.checkWritePermission(CONTEXT)) {
                    Utilities.askPermission(AddEditQuestionActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
                }

                if (Utilities.checkCameraPermission(CONTEXT) && Utilities.checkWritePermission(CONTEXT)) {
                    takePicture();
                }
                break;

            case R.id.action_gallery:
                MyLog.d(TAG, "Gallery button pressed");
                if (!Utilities.checkReadPermission(CONTEXT)) {
                    Utilities.askPermission(AddEditQuestionActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Constants.REQUEST_SELECT_IMAGE);
                }

                if (Utilities.checkWritePermission(CONTEXT)) {
                    selectPicture();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        ConstraintLayout cl = findViewById(R.id.constraint_layout_addQuestion);

        switch (requestCode) {

            case Constants.CODE_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permissions granted
                    Snackbar.make(cl, getResources().getString(R.string.write_permission_granted),
                            Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    // Permissions denied
                    Snackbar.make(cl, getResources().getString(R.string.write_permission_denied),
                            Snackbar.LENGTH_LONG)
                            .show();
                }
                break;

            case Constants.REQUEST_CAPTURE_IMAGE:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permissions granted
                    Snackbar.make(cl, getResources().getString(R.string.camera_permission_granted),
                            Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    // Permissions denied
                    Snackbar.make(cl, getResources().getString(R.string.camera_permission_denied),
                            Snackbar.LENGTH_LONG)
                            .show();
                }
                break;

            case Constants.REQUEST_SELECT_IMAGE:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permissions granted
                    Snackbar.make(cl, getResources().getString(R.string.read_permission_granted),
                            Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    // Permissions denied
                    Snackbar.make(cl, getResources().getString(R.string.read_permission_denied),
                            Snackbar.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return super.onNavigateUp();
    }

    public void takePicture() {
        try {
            // Se crea el directorio para las fotografías
            File dirFotos = new File(Constants.pathFotos);
            dirFotos.mkdirs();

            // Se crea el archivo para almacenar la fotografía
            File fileFoto = File.createTempFile(getFileCode(), ".jpg", dirFotos);

            // Se crea el objeto Uri a partir del archivo
            // A partir de la API 24 se debe utilizar FileProvider para proteger
            // con permisos los archivos creados
            // Con estas funciones podemos evitarlo
            // https://stackoverflow.com/questions/42251634/android-os-fileuriexposedexception-file-jpg-exposed-beyond-app-through-clipdata
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            uri = Uri.fromFile(fileFoto);
            MyLog.d(TAG, uri.getPath().toString());

            // Se crea la comunicación con la cámara
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Se le indica dónde almacenar la fotografía
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // Se lanza la cámara y se espera su resultado
            startActivityForResult(cameraIntent, Constants.REQUEST_CAPTURE_IMAGE);

        } catch (IOException ex) {

            MyLog.d(TAG, "Error: " + ex);
            CoordinatorLayout coordinatorLayout = findViewById(R.id.constraint_layout_addQuestion);
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, getResources().getString(R.string.error_files), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private String getFileCode() {
        // Se crea un código a partir de la fecha y hora
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss", java.util.Locale.getDefault());
        String date = dateFormat.format(new Date());
        // Se devuelve el código
        return "pic_" + date;
    }

    private void selectPicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        uri = intent.getData();
        startActivityForResult(
                Intent.createChooser(intent, getResources().getString(R.string.choose_picture)),
                Constants.REQUEST_SELECT_IMAGE
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {

            case Constants.REQUEST_CAPTURE_IMAGE:

                if (resultCode == Activity.RESULT_OK) {
                    // Se carga la imagen desde un objeto URI al imageView
                    ImageView imageView = findViewById(R.id.loadedImage);
                    Glide.with(this).load(uri).into(imageView);
                    addedImage = true;

                    // Se le envía un broadcast a la Galería para que se actualice
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(uri);
                    sendBroadcast(mediaScanIntent);
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // Se borra el archivo temporal
                    File file = new File(uri.getPath());
                    file.delete();
                }
                break;

            case (Constants.REQUEST_SELECT_IMAGE):
                if (resultCode == Activity.RESULT_OK) {
                    // Se carga la imagen desde un objeto Bitmap
                    Uri selectedImage = data.getData();
                    String selectedPath = selectedImage.getPath();

                    if (selectedPath != null) {
                        // Se leen los bytes de la imagen
                        InputStream imageStream = null;
                        try {
                            imageStream = getContentResolver().openInputStream(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        // Se transformam los bytes de la imagen a un Bitmap
                        Bitmap bmp = BitmapFactory.decodeStream(imageStream);

                        // Se carga el Bitmap en el ImageView
                        ImageView imageView = findViewById(R.id.loadedImage);
                        Glide.with(this).load(bmp).into(imageView);
                        addedImage = true;
                    }
                }
                break;
        }
    }

    private void initDialog() {
        alertDialog = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_delete_image, null);
        alertDialog.setView(view);
        alertDialog.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ImageView imageView = findViewById(R.id.loadedImage);
                imageView.setImageResource(R.drawable.ic_do_not_dry);
                addedImage = false;

                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyLog.d(TAG, "Starting onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);

        addedImage = false;

        final Bundle bundle = this.getIntent().getExtras();

        //If bundle is null sets edit to false, else sets it to true
        edit = bundle != null;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_edit_question);
        //Add UP button
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.add_question));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            MyLog.d(TAG, "Error loading toolbar");
        }

        final Spinner categoriesSpinner = findViewById(R.id.spinner_categories);

        final EditText statement = findViewById(R.id.question_input);
        //This sets the soft keyboard button to navigate to the next input, we'll use the same code
        //with the others.
        statement.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        statement.setRawInputType(InputType.TYPE_CLASS_TEXT);

        final EditText correctAnswer = findViewById(R.id.correct_answer_input);
        correctAnswer.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        correctAnswer.setRawInputType(InputType.TYPE_CLASS_TEXT);

        final EditText incorrectAnswer1 = findViewById(R.id.incorrect_answer_1_input);
        incorrectAnswer1.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        incorrectAnswer1.setRawInputType(InputType.TYPE_CLASS_TEXT);

        final EditText incorrectAnswer2 = findViewById(R.id.incorrect_answer_2_input);
        incorrectAnswer2.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        incorrectAnswer2.setRawInputType(InputType.TYPE_CLASS_TEXT);

        final EditText incorrectAnswer3 = findViewById(R.id.incorrect_answer_3_input);
        //On the last input, the soft keyboard button hides it
        incorrectAnswer3.setImeOptions(EditorInfo.IME_ACTION_DONE);
        incorrectAnswer3.setRawInputType(InputType.TYPE_CLASS_TEXT);

        final ImageView imageView = findViewById(R.id.loadedImage);
        imageView.setImageResource(R.drawable.ic_do_not_dry);

        //Filling the spinner for categories
        ArrayList<String> categories = new ArrayList<>();

        try {

            categories = DB_DRIVER.getCategories();

        } catch (SQLException ex) {

            //If there's an error in the database a snackbar will show an error message and
            //the activity will finish

            MyLog.d(TAG, "Database error: " + ex);
            MyLog.d(TAG, "Finishing activity...");

            findViewById(R.id.action_save).setEnabled(false);

            Snackbar.make(findViewById(android.R.id.content), R.string.db_error,
                    Snackbar.LENGTH_LONG).show();

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    finish();
                }
            }, 3000);
        }

        //After adding the categories to the ArrayList we add a "Add new category" element
        //and set a listener when we select this element

        final ArrayAdapter<String> adapter;
        if (categories.size() > 0) {

            categories.add(getResources().getString(R.string.new_category));

        } else {
            categories.add("");

            categories.add(getResources().getString(R.string.new_category));

        }

        adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,
                categories);

        categoriesSpinner.setAdapter(adapter);

        categoriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (categoriesSpinner.getSelectedItem().toString().equals(getResources().getString(
                        R.string.new_category))) {

                    MyLog.d(TAG, "Add new category selected");

                    //Recover the view of the AlertDialog from the activity layout
                    LayoutInflater layoutActivity = LayoutInflater.from(CONTEXT);
                    View viewAlertDialog = layoutActivity.inflate(R.layout.alert_dialog, null);

                    //Definition of the AlertDialog
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(CONTEXT);

                    //Set the view of the AlertDialog
                    alertDialog.setView(viewAlertDialog);

                    //Recovers the EditText of the AlertDialog
                    final EditText dialogInput = (EditText) viewAlertDialog.findViewById(
                            R.id.dialogInput);

                    //Configuration of the AlertDialog
                    alertDialog
                            .setCancelable(false)

                            //Add button
                            .setPositiveButton(getResources().getString(R.string.add),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogBox, int id) {

                                            if (dialogInput.getText().toString().equals(" ") ||
                                                    dialogInput.getText().toString().equals(
                                                            getResources().getString(
                                                                    R.string.new_category))) {

                                                Snackbar.make(findViewById(android.R.id.content),
                                                        R.string.not_permitted_category,
                                                        Snackbar.LENGTH_LONG).show();

                                                categoriesSpinner.setSelection(adapter.getPosition(" "));

                                            } else {
                                                MyLog.d(TAG, "Adding...");
                                                adapter.remove(" ");
                                                adapter.remove(getResources().getString(R.string
                                                        .new_category));
                                                adapter.add(dialogInput.getText().toString());
                                                adapter.add(getResources().getString(R.string
                                                        .new_category));
                                                categoriesSpinner.setSelection(adapter.getPosition(
                                                        dialogInput.getText().toString()));
                                            }
                                            //Cancel button
                                        }
                                        //Cancel button
                                    }).setNegativeButton(getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    MyLog.d(TAG, "Cancelled...");
                                    //Checks if we are editing or creating a question
                                    if (!edit) {
                                        categoriesSpinner.setSelection(0);
                                    } else {
                                        categoriesSpinner.setSelection(adapter.getPosition(
                                                DB_DRIVER.getQuestion(bundle.getString("id")).getCategory()));
                                    }
                                }
                            })
                            .create().show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //If the bundle isn't null searches the question from the database and set the values
        //of the text fields.
        if (bundle != null) {

            try {

                Question q = DB_DRIVER.getQuestion(bundle.getString("id"));

                categoriesSpinner.setSelection(adapter.getPosition(q.getCategory()));

                statement.setText(q.getStatement());

                correctAnswer.setText(q.getAnswers().get(0).getAnswer());

                incorrectAnswer1.setText(q.getAnswers().get(1).getAnswer());

                incorrectAnswer2.setText(q.getAnswers().get(2).getAnswer());

                incorrectAnswer3.setText(q.getAnswers().get(3).getAnswer());

                Bitmap bitmap = Utilities.base64ToImg(q.getImage());
                if (bitmap != null) {
                    Glide.with(this).load(bitmap).into(imageView);
                    addedImage = true;
                } else {
                    imageView.setImageResource(R.drawable.ic_do_not_dry);
                }

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (addedImage) {
                            initDialog();
                            alertDialog.show();
                        }
                    }
                });

            } catch (SQLException ex) {

                //If there's an error getting the question the activity finish
                MyLog.d(TAG, "Error getting question: " + ex);
                MyLog.d(TAG, "Finishing activity...");
                findViewById(R.id.action_save).setEnabled(false);
                Snackbar.make(findViewById(android.R.id.content), R.string.get_question_error,
                        Snackbar.LENGTH_LONG).show();

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 3000);
            }
        }
        MyLog.d(TAG, "Finishing onCreate");
    }

    @Override
    protected void onStart() {

        MyLog.d(TAG, "Staring onStart");

        super.onStart();

        MyLog.d(TAG, "Finishing onStart");
    }

    @Override
    protected void onResume() {

        MyLog.d(TAG, "Starting onResume");

        super.onResume();

        MyLog.d(TAG, "Finishing onResume");
    }

    @Override
    protected void onPause() {

        MyLog.d(TAG, "Starting onPause");

        super.onPause();

        MyLog.d(TAG, "Finishing onPause");
    }

    @Override
    protected void onStop() {

        MyLog.d(TAG, "Starting onStop");

        super.onStop();

        MyLog.d(TAG, "Finishing onStop");
    }

    @Override
    protected void onDestroy() {

        MyLog.d(TAG, "Starting onDestroy");

        super.onDestroy();

        MyLog.d(TAG, "Finishing onDestroy");
    }
}
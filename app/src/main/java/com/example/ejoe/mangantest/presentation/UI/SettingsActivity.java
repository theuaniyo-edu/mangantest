package com.example.ejoe.mangantest.presentation.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.ejoe.mangantest.utilities.MyLog;
import com.example.ejoe.mangantest.R;
import com.example.ejoe.mangantest.presentation.presenters.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    private final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyLog.d(TAG, "Starting onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

        MyLog.d(TAG, "Finishing onCreate");
    }

    @Override
    protected void onStart() {

        MyLog.d(TAG, "Starting onStart");

        super.onStart();

        MyLog.d(TAG, "Finishing onStart");
    }

    @Override
    protected void onResume() {

        MyLog.d(TAG, "Starting onResume");

        super.onResume();

        MyLog.d(TAG, "Finishing onResume");
    }

    @Override
    protected void onPause() {

        MyLog.d(TAG, "Starting onPause");

        super.onPause();

        MyLog.d(TAG, "Finishing onPause");
    }

    @Override
    protected void onStop() {

        MyLog.d(TAG, "Starting onStop");

        super.onStop();

        MyLog.d(TAG, "Finishing onStop");
    }

    @Override
    protected void onDestroy() {

        MyLog.d(TAG, "Starting onDestroy");

        super.onDestroy();

        MyLog.d(TAG, "Finishing onDestroy");
    }
}

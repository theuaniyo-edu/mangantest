package com.example.ejoe.mangantest.utilities;

import android.os.Environment;

public class Constants {

    public static final int CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int REQUEST_CAPTURE_IMAGE = 2;
    public static final int REQUEST_SELECT_IMAGE = 3;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 4;
    public static final int CODE_READ_EXTERNAL_STORAGE_PERMISSION = 5;
    public static final String pathFotos = Environment.getExternalStorageDirectory().getPath() + "/Mangantest/";
    public static final int VERSION = 1;
    public static final String DB = "Mangantest.db";
    public static final String QUESTION_TABLE = "Questions";
}

package com.example.ejoe.mangantest.Glide;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class MangantestGlideModule extends AppGlideModule {}

package com.example.ejoe.mangantest.storage.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

/**
 * This class is used to create the database used with the app
 */
public class ManganTestSQLite extends SQLiteOpenHelper{

    private final String sqlCreate = "CREATE TABLE questions (statement TEXT, category TEXT, " +
            "correctAnswer TEXT, incorrectAnswer1 TEXT, incorrectAnswer2 TEXT, " +
            "incorrectAnswer3 TEXT, image TEXT)";

    public ManganTestSQLite(@Nullable Context context, @Nullable String name, @Nullable CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS preguntas");
        db.execSQL(sqlCreate);
    }
}

package com.example.ejoe.mangantest.presentation.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.ejoe.mangantest.utilities.MyLog;
import com.example.ejoe.mangantest.R;

import java.util.Objects;

public class AboutActivity extends AppCompatActivity {

    private final String TAG = "AboutActivity";

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return super.onNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyLog.d(TAG, "Starting onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


        Toolbar toolbar = (Toolbar) findViewById(R.id.about_toolbar);

        //Adds UP button
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.about));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            MyLog.d(TAG, "Error loading toolbar");
        }

        MyLog.d(TAG, "Finishing onCreate");
    }

    @Override
    protected void onStart() {

        MyLog.d(TAG, "Starting onStart");

        super.onStart();

        MyLog.d(TAG, "Finishing onStart");
    }

    @Override
    protected void onResume() {

        MyLog.d(TAG, "Starting onResume");

        super.onResume();

        MyLog.d(TAG, "Finishing onResume");
    }

    @Override
    protected void onPause() {

        MyLog.d(TAG, "Starting onPause");

        super.onPause();

        MyLog.d(TAG, "Finishing onPause");
    }

    @Override
    protected void onStop() {

        MyLog.d(TAG, "Starting onStop");

        super.onStop();

        MyLog.d(TAG, "Finishing onStop");
    }

    @Override
    protected void onDestroy() {

        MyLog.d(TAG, "Starting onDestroy");

        super.onDestroy();

        MyLog.d(TAG, "Finishing onDestroy");
    }
}
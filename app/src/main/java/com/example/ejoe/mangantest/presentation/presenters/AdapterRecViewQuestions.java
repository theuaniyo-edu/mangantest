package com.example.ejoe.mangantest.presentation.presenters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ejoe.mangantest.R;
import com.example.ejoe.mangantest.model.Question;

import java.util.ArrayList;

public class AdapterRecViewQuestions extends RecyclerView.Adapter<AdapterRecViewQuestions.QuestionsViewHolder>
        implements View.OnClickListener {

    private ArrayList<Question> questionsList;
    private View.OnClickListener listener;

    public AdapterRecViewQuestions(ArrayList<Question> questionsList) {
        this.questionsList = questionsList;
    }

    @NonNull
    @Override
    public QuestionsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.row, viewGroup,false);
        row.setOnClickListener(this);
        QuestionsViewHolder qvh = new QuestionsViewHolder(row);
        return qvh;
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionsViewHolder questionsViewHolder, int i) {
        Question q = questionsList.get(i);
        questionsViewHolder.questionBind(q);
    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public static class QuestionsViewHolder extends RecyclerView.ViewHolder{

        private TextView statement;
        private TextView category;

        public QuestionsViewHolder(@NonNull View itemView) {
            super(itemView);

            statement = (TextView) itemView.findViewById(R.id.statement_txt);
            category = (TextView) itemView.findViewById(R.id.category_txt);

        }

        public void questionBind(Question q) {
            statement.setText(q.getStatement());
            category.setText(q.getCategory());
        }
    }

    public void removeItem(int position) {
        questionsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, questionsList.size());
    }
}

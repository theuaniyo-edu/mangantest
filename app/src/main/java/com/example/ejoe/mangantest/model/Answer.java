package com.example.ejoe.mangantest.model;

import java.util.Objects;

public class Answer {

    /**
     * The sentence of the answer
     */
    private String answer;
    /**
     * Tells if the answer is correct or not
     */
    private boolean correct;

    public Answer(){

    }

    /**
     * Creates a new Answer object. If the argument is a null object throws a NullPointerException.
     * @param answer the sentence of the answer
     * @throws NullPointerException if the argument is a null object.
     */
    public Answer(String answer) throws NullPointerException{
        Objects.requireNonNull(answer);
        this.answer = answer;
    }

    /**
     * Creates a new Answer object. If the arguments are a null object throws a NullPointerException.
     * @param answer the sentence of the answer
     * @param correct if the answer is correct or not
     * @throws NullPointerException if any argument is a null object
     */
    public Answer(String answer, boolean correct) throws NullPointerException{
        Objects.requireNonNull(answer);
        this.answer = answer;
        this.correct = correct;
    }

    /**
     *
     * @return the statement of the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     *
     * @param answer the statement of the answer
     */
    public void setAnswer(String answer) {
        if (answer == null || answer.equals("")){
            throw new NullPointerException("Null statement");
        } else {
            this.answer = answer;
        }
    }

    /**
     *
     * @return if the answer is the correct one or not
     */
    public boolean isCorrect() {
        return correct;
    }

    /**
     *
     * @param correct if the answer is correct or not
     */
    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}

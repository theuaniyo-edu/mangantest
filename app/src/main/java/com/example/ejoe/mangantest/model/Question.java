package com.example.ejoe.mangantest.model;

import java.util.ArrayList;
import java.util.Objects;

public class Question {

    /**
     * The statement of the question
     */
    private String statement;
    /**
     * The posible answers for the question
     */
    private ArrayList<Answer> answers;
    /**
     * The id of the question stored in the database
     */
    private String id;
    /**
     * The category that the question belongs to
     */
    private String category;
    private String image;

    public Question(){
        answers = new ArrayList<>();
    }

    public Question(String statement, String category, Answer correctAnswer,
                    Answer incorrectAnswer1, Answer incorrectAnswer2, Answer incorrectAnswer3,
                    String image) throws NullPointerException{

        Objects.requireNonNull(statement);
        this.statement = statement;

        Objects.requireNonNull(category);
        this.category = category;

        answers = new ArrayList<>();

        Objects.requireNonNull(correctAnswer);
        answers.add(correctAnswer);

        Objects.requireNonNull(incorrectAnswer1);
        answers.add(incorrectAnswer1);

        Objects.requireNonNull(incorrectAnswer2);
        answers.add(incorrectAnswer2);

        Objects.requireNonNull(incorrectAnswer3);
        answers.add(incorrectAnswer3);

        this.image = image;
    }

    /**
     * Creates a Question object. Used when editing a question. If any of the arguments, except for
     * image, is null it will throw a NullPointerException.
     * @param rowId the id of the question stored in the database
     * @param statement the statement of the question
     * @param category the category that the question belongs to
     * @param correctAnswer the correct answer
     * @param incorrectAnswer1 an incorrect answer
     * @param incorrectAnswer2 an incorrect answer
     * @param incorrectAnswer3 an incorrect answer
     * @throws NullPointerException if any of the arguments is null (except for image)
     */
    public Question(String rowId, String statement, String category, Answer correctAnswer,
                    Answer incorrectAnswer1, Answer incorrectAnswer2, Answer incorrectAnswer3,
                    String image) throws NullPointerException{

        Objects.requireNonNull(rowId);
        this.id = rowId;

        Objects.requireNonNull(statement);
        this.statement = statement;

        Objects.requireNonNull(category);
        this.category = category;

        answers = new ArrayList<>();

        Objects.requireNonNull(correctAnswer);
        answers.add(correctAnswer);

        Objects.requireNonNull(incorrectAnswer1);
        answers.add(incorrectAnswer1);

        Objects.requireNonNull(incorrectAnswer2);
        answers.add(incorrectAnswer2);

        Objects.requireNonNull(incorrectAnswer3);
        answers.add(incorrectAnswer3);

        this.image = image;
    }

    /**
     *
     * @return a String with the attributes of the Question object in query form
     */
    public String toQuery() {
        return "'" + statement + "', '" + category + "', '" + answers.get(0).getAnswer() + "', '"
                + answers.get(1).getAnswer() + "', '" + answers.get(2).getAnswer()
                + "', '"  + answers.get(3).getAnswer() + "'";
    }

    /**
     * Adds an Answer object to the answers array. If there are four answers already it will
     * throw an IndexOutOfBoundsException
     * @param a the answer to add
     * @return true if the question is added and false if not
     * @throws IndexOutOfBoundsException if there are four answers in the array already
     */
    public boolean addAnswer(Answer a) throws IndexOutOfBoundsException{
        if (answers.size() == 4){
            throw new IndexOutOfBoundsException();
        }
        return answers.add(a);
    }

    /**
     * Removes a question from the array
     * @param a the answer to add
     * @return true if the answer is added and false if not.
     */
    public boolean removeAnswer(Answer a){
        return answers.remove(a);
    }

    /**
     *
     * @return the statement of the answer
     */
    public String getStatement() {
        return statement;
    }

    /**
     *
     * @param statement the statement of the answer
     */
    public void setStatement(String statement) {
        if (statement == null || statement.equals("")){
            throw new NullPointerException("Null statement");
        } else {
            this.statement = statement;
        }
    }

    /**
     *
     * @return an ArrayList with the posible answers for the question
     */
    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    /**
     *
     * @param answers an ArrayList with the posible answers for the question
     */
    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    /**
     *
     * @return the category that the question belongs to
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category the category that the question belongs to
     */
    public void setCategory(String category) {
        if (category == null || category.equals("")) {
            throw new NullPointerException("Null category");
        } else {
            this.category = category;
        }
    }

    /**
     *
     * @return the id of the question
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id the id of the question
     */
    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
